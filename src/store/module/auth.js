//import global API

import Api from '../../api/Api'

const auth = {

    //set namespace true
    namespaced: true,

    //state
    state: {

        //state untuk token, menggunakan local storage, untuk menyimpan informasi token
        token: localStorage.getItem('token') || '',

        //state user, menggunakan local storage, untuk menyimpan data user yang sedang login
        user: JSON.parse(localStorage.getItem('user')) || {},

    },

    //mutations
    mutations: {

        //update state token dan state user dari hasil response
        AUTH_SUCCESS(state, token, user){
            state.token = token // <-- assign state token dengan response token
            state.user  = user // <-- assign state user dengan response user
        },

        //update state user dari hasil response register/login
        GET_USER(state, user) {
            state.user = user // <-- assign state user dengan response data user
        },

        //fungsi logout
        AUTH_LOGOUT(state) {
            state.token = '' // <-- set state token ke empty
            state.user = '' // <-- set state user ke empty
        },

    },

    //actions
    actions: {

        //action register
        register({ commit }, user){

            //define callback promise
            return new Promise((resolve, reject) => {

                //kirim data ke server
                Api.post('/register', {

                    //data yang dikirim ke server untuk proses register
                    name: user.name,
                    email: user.email,
                    password: user.password,
                    password_confirmation: user.password_confirmation

                })

                .then(response => {

                    //define variabel dengan isi hasil response dari server
                    const token = response.data.token
                    const user = response.data.data

                    //set local storage untuk menyimpan token dan data user
                    localStorage.setItem('token', token)
                    localStorage.setItem('user', JSON.stringify(user))

                    //set default header axios dengan token
                    Api.defaults.headers.common['Authorization'] = `Bearer ${token}`

                    //commit auth success ke mutation
                    commit('AUTH_SUCCESS', token, user)

                    //commit get user ke mutation
                    commit('GET_USER', user)

                    //resolve ke component dengan hasil response
                    resolve(response)
                
                }).catch(error => {

                    //jika gagal, remove local storage dengan key token
                    localStorage.removeItem('token')

                    //reject ke component dengan hasil response
                    reject(error.response.data)

                })
            })
        },

        //action getUser
        getUser({ commit }) {

            //ambil data token dari localStorage
            const token = localStorage.getItem('token')

            Api.defaults.headers.common['Authorization'] = `Bearer ${token}`
            Api.get('/user')
            .then(response => {

                //commit ke mutation GET_USER dengan hasil response
                commit('GET_USER', response.data)
            })
        },

        //action logout
        logout({ commit }) {

            //define callback promise
            return new Promise((resolve) => {

                //commit ke mutation AUTH_LOGOUT
                commit('AUTH_LOGOUT')

                //hapus value dari local storage
                localStorage.removeItem('token')
                localStorage.removeItem('user')

                //diatas kita set datanya menjadi 0

                //delete header axios
                delete Api.defaults.headers.common['Authorization']

                //return resolve ke component
                resolve()

            })
        },

        //action login
        login({ commit }, user) {

            //define callback promise
            return new Promise((resolve, reject) => {

                Api.post('/login', {

                    //data yang dikirim ke server
                    email: user.email,
                    password: user.password,

                })
                .then(response => {

                    //define variable dengan hasil response dari server
                    const token = response.data.token
                    const user = response.data.data

                    //set localStorage untuk menyimpan token dan data user
                    localStorage.setItem('token', token)
                    localStorage.setItem('user', JSON.stringify(user))

                    //set default header bearer dengan axios
                    Api.defaults.headers.common['Authorization'] = `Bearer ${token}`

                    //commit auth success ke mutation
                    commit('AUTH_SUCCESS', token, user)

                    //commit get user ke mutation
                    commit('GET_USER', user)

                    //resolve ke component dengan hasil response
                    resolve(response)

                }).catch(error => {

                    //jika gagal remove localStorage dengan key token
                    localStorage.removeItem('token')

                    //reject ke component dengan hasil response
                    reject(error.response.data)
                })
            })
        }

    },

    

    //getters
    getters: {

        //get current user
        currentUser(state) {
            return state.user // <-- return dengan data user
        },

        //loggedIn
        isLoggedIn(state) {
            return state.token // return dengan data token
        },

    }
}

export default auth